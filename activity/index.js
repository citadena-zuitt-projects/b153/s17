function addStudent(name){
	if(typeof name === 'string'){
		console.log(`${name} was added to the student's list.`)
		studentList.push(name)
	} else {
		console.log("Please input a valid name.")
	}
};

let studentList = []

function countStudents(){
	console.log("There are a total of " + studentList.length + " students enrolled.")
};

function printStudents(){
	studentList.forEach(function(studentName){
		console.log(`${studentName}`)
	})
};

function addSection(sectionCode){
	newSection = studentList.join(` - section ${sectionCode}, `)
	console.log(newSection)
};

// I don't know how to make these not case sensitive tho

function removeStudent(deleteStudent){
	x = studentList.indexOf(deleteStudent)
	studentList.splice(x, 1)
	console.log(`${deleteStudent} was removed from the student's list.`)
};

function findStudent(student){
	if(studentList.indexOf(student) !== -1){
		console.log(`${student} is an Enrollee.`)
	} else {
		console.log(`No student found with the name ${student}.`)
	};
};